<?php
$result = null;
$text = $_POST['text'] ?? '';

if (!empty($text)) {
    $datetime = date(DATE_ATOM);
    $isWrote = file_put_contents(__DIR__ . '/private/feedback.txt', $datetime . PHP_EOL . $text . PHP_EOL, FILE_APPEND) !== false;
    if ($isWrote === false) {
        $result = 'Не удалось отправить сообщение';
    } else {
        $result = 'Ваше сообщение отправлено';
    }
}
?>
<html lang="en">
<head>
    <title>Форма обратно связи</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="background">
        <h1>Форма обратной связи</h1>
        <?php if ($result !== null): ?>
        <div><b><?= $result ?></b></div>
        <?php endif; ?>
        <form action="form.php" method="post">
            <div class="input-form">
                <label class="text-form" for="text">Введите ваш текст</label><br>
                <br>
                <textarea class="textarea"  name="text" id="text" cols="40" rows="20"></textarea>
                <br>
                <br>
                <input type="submit" class="btn" value="Отправить">
            </div>
        </form>
    </div>
</body>
</html>
